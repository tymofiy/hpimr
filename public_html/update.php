<?php

$hpimr_project_id = 8633285;
$search_for_job = 'combine';

function write($w)
{
    echo $w;
    static $handle = null;
    if (!$handle) {
        $handle = fopen('../update.log', 'a');
        if (!$handle) {
            exit(1);
        }
        fwrite($handle, "==========\n");
        fwrite($handle, date('Y-m-d H:i:s') . "\n");
    }
    fwrite($handle, $w);
}

function exec_and_get($command)
{
    $output = '';
    write("Executing $command:\n");
    exec($command, $ar, $ret);
    foreach ($ar as $v) {
        $output = "$output$v\n";
    }
    write("$output\n");
    write("Return value: $ret");
    return [$output, $ret];
}

$pipeline_id = $_GET['gitlab_pipeline'];
$token = $_GET['auth'];

write("Got request pipeline: '$pipeline_id', token: '$token'\n");

if (!is_numeric($pipeline_id)) {
    write("ERROR Pipeline is not numeric\n");
    exit(1);
}

$real_token = trim(file_get_contents('../token'));
if ($real_token != $token) {
    write("ERROR Wrong token!\n");
    exit(1);
}
$access_token = trim(file_get_contents('../access_token'));

$command = "curl -s --header 'PRIVATE-TOKEN: $access_token'  'https://gitlab.com/api/v4/projects/$hpimr_project_id/pipelines/$pipeline_id/jobs'";
$r = exec_and_get($command);
if ($r[1]) {
    exit($r[1]);
}
$response = json_decode($r[0]);

$job_id = 0;
foreach ($response as $job) {
    if ($job->name != $search_for_job) {
        continue;
    }
    $job_id = $job->id;
    break;
}
if ($job_id == 0) {
    write("Could not find job!");
    exit(1);
}

write("all ok, found job $job_id, updating\n");
$command = "wget https://gitlab.com/hpimr/hpimr/-/jobs/$job_id/artifacts/download -O ../new.zip";
$command = "$command && cd .. && unzip -o new.zip && rm new.zip";
$r = exec_and_get($command);
exit($r[1]);
