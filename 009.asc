// 1238
//== Title Redacted, Part I
== Розділ 9. Назву приховано, частина I

// 1239
//All your base are belong to J. K. Rowling.
// тут мем: https://en.wikipedia.org/wiki/All_your_base_are_belong_to_us
Усі ваші база належать Дж. К. Ролінґ.

***

// 1240
//1,000 REVIEWS IN 26 DAYS WOOHOO AWESOME POWA! 30 DAYS 1,189 REVIEWS COMBO IS
//CONTINUING! YEAH! YOU PEOPLE ARE THE BEST! THIS IS SPARTAAAAA!
1000 РЕВʼЮ ЗА 26 ДНІВ, ЯХУ, КРУТО, СУПЕР! 30 ДНІВ -- 1189 РЕВʼЮ, КОМБО І ЗБІЛЬШУЄТЬСЯ!
ТАК! ЛЮДИ, ВИ НАЙКРАЩІ! ЦЕ СПАРТА-А-А-А-А!
// 1241
//Ahem.
// 1242
//The third-generation quarks were also called “truth” and “beauty” before “top”
//and “bottom” won out; my birthdate is around Hermione’s, and when I was eleven,
//I used “truth” and “beauty”.
// 1243
//When Part I of this chapter was first posted, I said that if anyone guessed
//what the last sentence was talking about before the next update, I would tell
//them the entire rest of the plot.

***

// 1244
//__You never did know what tiny event might upset the course of your
//master plan.__
__Ніколи не знаєш, яка дрібна подія може зіпсувати перебіг твого геніального
плану.__

***

// 1245
//“Abbott, Hannah!”
-- Анна Ебот!

// 1246
//Pause.
Павза.

// 1247
//“HUFFLEPUFF!”
-- ГАФЕЛПАФ!

// 1248
//“Bones, Susan!”
-- Сьюзен Боунз!

// 1249
//Pause.
Павза.

// 1250
//“HUFFLEPUFF!”
-- ГАФЕЛПАФ!

// 1251
//“Boot, Terry!”
-- Террі Бут!

// 1252
//Pause.
Павза.

// 1253
//“RAVENCLAW!”
РЕЙВЕНКЛОВ!

// 1254
//Harry glanced over briefly at his new House-mate, more to get a quick look at
//the face than anything else. He was still trying to get himself under control
//from his encounter with the ghosts. The sad, the really sad, the really truly
//sad thing was that he __did__ seem to be getting himself under control
//again. It seemed ill-fitting. Like he should have taken at least a day. Maybe a
//whole lifetime. Maybe just never.
Гаррі зиркнув на свого нового однокласника -- просто хотів розгледіти його обличчя.
Гаррі досі намагався оговтатися після зустрічі
з привидами. Сумним, дуже сумним, насправжки дуже сумним було те, що, схоже, він
починав опановувати себе знову. Це здавалося недоречним. Так, наче це мало
тривати хоча б день. Чи все життя. А може, так ніколи й не трапитися.

// 1255
//“Corner, Michael!”
-- Майкл Корнер!

// 1256
//Long pause.
Довга павза.

// 1257
//“RAVENCLAW!”
-- РЕЙВЕНКЛОВ!

// 1258
//At the lectern before the huge Head Table stood Professor McGonagall, looking
//sharp and looking sharply around, as she called out one name after another,
//though she had smiled only for Hermione and a few others. Behind her, in the
//tallest chair at the table - really more of a golden throne - sat a wizened and
//bespectacled ancient, with a silver-white beard that looked like it would go
//almost to the floor if it were visible, watching over the Sorting with a
//benevolent expression; as stereotypical in appearance as a Wise Old Man could
//possibly be, without actually being Oriental. (Though Harry had learned to be
//wary of stereotypical appearances from the first time he’d met Professor
//McGonagall and thought that she ought to cackle.) The ancient wizard had
//applauded every student Sorted, with an unwavering smile that somehow seemed
//freshly delighted for each.
// @ Head Table - Аліна: у каноні начебто учительський стіл. Не знаю, чи витримано в усіх книжках,
// але пропоную тут дотримуватися такого формулювання всюди
За катедрою перед величезним учительським столом височіла професорка Макґонеґел, що
мала суворий вигляд і суворо роздивлялася довкола. Вона виклика{sress}ла учнів одного за
одним, хоча усміхнулася лише Герміоні й декільком іншим. За нею на найвищому
за столом стільці  -- більше схожому на золотий трон -- сидів висохлий старець в
окулярах і зі срібно-білою бородою, що, певно, торкалася б підлоги, якби
її було видно; він доброзичливо спостерігав за сортуванням. Його вигляд був
настільки стереотипним, наскільки це взагалі можливо для Мудрого Старця, тільки
не зі Сходу. (Хоча Гаррі навчився був ставитися з недовірою до стереотипної
зовнішности, відколи зустрів професорку Макґонеґел і подумав, що та має ґелґотати.)
Древній чарівник аплодував кожнісінькому розподіленому учневі з непохитною
усмішкою, що якимось дивом здавалася наново щиро щасливою для кожного.

// 1259
//To the golden throne’s left side was a man with sharp eyes and a dour face who
//had applauded no-one, and who somehow managed to be looking straight back at
//Harry every time Harry looked at him. Further to the left, the pale-faced man
//Harry had seen in the Leaky Cauldron, whose eyes darted around as though in
//panic at the surrounding crowd, and who seemed to occasionally jerk and twitch
//in his seat; for some reason Harry kept finding himself staring at him. To that
//man’s left, a string of three older witches who didn’t seem much interested in
//the students. Then to the right side of the tall golden chair, a round-faced
//middle-aged witch with a yellow hat, who had applauded every student except the
//Slytherins. A tiny man standing on his chair, with a poofy white beard, who had
//applauded every student, but smiled only upon the Ravenclaws. And on the
//farthest right, occupying the same space as three lesser beings, the
//mountainous entity who’d greeted them all after they’d disembarked from the
//train, naming himself Hagrid, Keeper of Keys and Grounds.
// @ Keeper of Keys and Grounds
Ліворуч від золотого трону сидів чоловік із пронизливими очима й похмурим
обличчям, що нікому не аплодував; чомусь щоразу, коли Гаррі дивився на
нього, той уже дивився у відповідь. Далі ліворуч виднівся чоловік із блідим обличчям,
якого Гаррі зустрів був у "`Дірявому казані`". Його очі роззиралися навколо натовпу,
ніби в паніці, здавалося, що він подеколи сіпається й смикається на стільці;
з незрозумілої причини Гаррі постійно ловив себе на тому, що витріщався на
нього. Ще ліворуч розмістилися поруч три старі відьми; учні їх начебто не цікавили.
Праворуч від золотого стільця була повновида відьма середнього віку в
жовтому капелюсі, що аплодувала кожному учневі, крім слизеринців. Далі на стільці
стояв крихітний чоловічок із пишною білою бородою -- він аплодував
усім, проте усміхався лише рейвенкловцям. А останнім праворуч,
займаючи стільки ж місця, скільки й три менші істоти, бовваніло величезне створіння, що вітало було
їх усіх після того, як вони зійшли з потяга, й називало себе Геґрідом,
ключником і охоронцем дичини.

// 1260
//“Is the man standing on his chair the Head of Ravenclaw?” Harry whispered
//towards Hermione.
-- Чоловік, що стоїть на стільці, -- це вихователь Рейвенклову? -- прошепотів
Гаррі до Герміони.

// 1261
//For once Hermione didn’t answer this instantly; she was shifting constantly
//from side to side, staring at the Sorting Hat, and fidgeting so energetically
//that Harry thought her feet might be leaving the floor.
Вперше Герміона не відповіла на його питання миттєво; вона переминалася з ноги
на ногу, витріщалася на Сортувальний Капелюх і дуже енергійно вертілася --
Гаррі аж здалося, що та може відірватися від підлоги.

// 1262
//“Yes, he is,” said one of the prefects who’d accompanied them, a young woman
//wearing the blue of Ravenclaw. Miss Clearwater, if Harry recalled correctly.
//Her voice was quiet, but conveyed a tinge of pride. “That is the Charms
//Professor of Hogwarts, Filius Flitwick, the most knowledgeable Charms Master
//alive, and a past Duelling Champion -”
// @ Duelling Champion
-- Так, це він, -- підтвердила одна зі старост, що супроводжували їх, юна дівчина в одязі
синього кольору Рейвенклову. Міс Клірвотер, якщо Гаррі правильно запамʼятав. Її
голос був тихим, проте відверто гордим. -- Це професор чарів Гоґвортсу Філіус Флитвік,
найбільш обізнаний викладач цього предмета з нині живих, чемпіон-дуелянт у минулому...

// 1263
//“Why’s he so __short?__” hissed a student whose name Harry didn’t recall.
//“Is he a __halfbreed?__”
-- А чого він такий __низький__? -- прошипів учень, імʼя котрого Гаррі не міг
пригадати. -- Він __напівкровка__?

// 1264
//A chill glance from the young lady prefect. “The Professor does indeed have
//goblin ancestry -”
-- Серед предків професора й справді були ґобліни... -- обдарувала його крижаним
поглядом панна староста.

// 1265
//“What?” Harry said involuntarily, causing Hermione and four other students to
//hush him.
-- Що? -- мимоволі вихопилося в Гаррі, через що Герміона й чотири інші учні цитьнули
на нього.

// 1266
//Now Harry was getting a surprisingly intimidating glare from the Ravenclaw
//prefect.
Тепер уже йому дістався напрочуд загрозливий погляд від старости Рейвенклову.

// 1267
//“I mean -” Harry whispered. “Not that I have a __problem__ with that -
//it’s just - I mean - how’s that __possible?__ You can’t just mix two
//different species together and get viable offspring! It ought to scramble the
//genetic instructions for every organ that’s different between the two species -
//it’d be like trying to build,” they didn’t have cars so he couldn’t use a
//scrambled-engine-blueprints analogy, “a half-carriage half-boat or something…”
-- Тобто... -- пробубонів Гаррі. -- Не те щоб я мав якісь __проблеми__ з цим.
Просто, тобто... Як таке __можливо__? Не можна просто змішати два різні
види разом і здобути життєздатних нащадків! Має утворитися мішанина генетичних
інструкцій для кожного органа, що є різним у двох видів. Це ніби намагатися
побудувати, -- у них немає автівок, отже, не вдасться використати аналогію
переплутаних частин схем двигунів, -- напівкарету-напівчовен чи щось...

// 1268
//The Ravenclaw prefect was still looking at Harry severely. “Why
//__couldn’t__ you have a half-carriage half-boat?”
-- А чому не можна побудувати напівкарету-напівчовен? -- староста
досі свердлила його поглядом.

// 1269
//“__Hssh!__” hsshed another prefect, though the Ravenclaw witch had still
//spoken quietly.
-- __Ш-ш-ш!__ -- зашикав інший староста, хоча рейвенкловська відьма й так
розмовляла тихо.

// 1270
//“I mean -” Harry said even more quietly, trying to figure out how to ask
//whether goblins had evolved from humans, or evolved from a common ancestor of
//humans like __Homo erectus__, or if goblins had been __made__ out of
//humans somehow - if, say, they were still genetically human under a heritable
//enchantment whose magical effect was diluted if only one parent was a ‘goblin’,
//which would explain how interbreeding was possible, and in which case goblins
//would __not__ be an incredibly valuable second data point for how
//intelligence had evolved in other species besides __Homo sapiens__ - now
//that Harry thought about it, the goblins in Gringotts __hadn’t__ seemed
//very much like genuinely alien, nonhuman intelligences, nothing like Dirdir or
//Puppeteers - “I mean, where did goblins __come__ from, anyway?”
-- Маю на увазі... -- озвався Гаррі ще тихіше.

// @ Dirdir Puppeteers -- вирішив не перекладати, адже це власні назви
Він намагався вигадати спосіб запитати, чи ґобліни походять від людей, чи
еволюціонували від спільного з людьми предка, наприклад Homo erectus; а чи
ґоблінів якось __створили__ з людей. Якщо, ну, вони досі генетично
люди під спадковим заклинанням, магічний ефект якого зменшується в разі, коли лише
один із батьків був "`ґобліном`", це пояснило б можливість схрещування. Тоді
ґобліни __не є__ неймовірно цінним другим спостереженням щодо того, як виник
інтелект у якомусь виді, крім Homo sapiens. Коли Гаррі задумався про це тепер,
то ґобліни з "`Ґрінґотсу`" не здавалися були якимись незбагненними
нелюдськими інтелектами, зовсім не як Дірдір чи Папетіри.

-- Маю на увазі, гаразд, звідки ґобліни __взялися__?

// 1271
//“Lithuania,” Hermione whispered absently, her eyes still fixed firmly on the
//Sorting Hat.
-- З Литви, -- неуважно прошелестіла Герміона, все ще не відриваючи очей від
Сортувального Капелюха.

// 1272
//Now Hermione was getting a smile from the lady prefect.
Тепер Герміона удостоїлася усмішки від панни старости.

// 1273
//“Never mind,” whispered Harry.
-- Забудьте, -- пошепки сказав Гаррі.

// 1274
//At the lectern, Professor McGonagall called out, “Goldstein, Anthony!”
// @ Goldstein Anthony
-- Ентоні Ґольдштейн! -- покликала з-за катедри професорка Макґонеґел.

// 1275
//“RAVENCLAW!”
-- РЕЙВЕНКЛОВ!

// 1276
//Hermione, next to Harry, was bouncing on her tiptoes so hard that her feet were
//actually leaving the ground on each bounce.
Поруч із Гаррі Герміона аж витанцьовувала на кінчиках пальців ніг,
із кожним рухом відриваючись від землі.

// 1277
//“Goyle, Gregory!”
-- Ґреґорі Ґойл!

// 1278
//There was a long, tense moment of silence under the Hat. Almost a minute.
Запала довга, напружена тиша під Капелюхом. Майже на хвилину.

// 1279
//“SLYTHERIN!”
-- СЛИЗЕРИН!

// 1280
//“Granger, Hermione!”
-- Герміона Ґрейнджер!

// 1281
//Hermione broke loose and ran full tilt towards the Sorting Hat, picked it up
//and jammed the patchy old clothwork down hard over her head, making Harry
//wince. Hermione had been the one to explain to __him__ about the Sorting
//Hat, but she certainly didn’t __treat__ it like an irreplaceable, vitally
//important, 800-year-old artefact of forgotten magic that was about to perform
//intricate telepathy on her mind and didn’t seem to be in very good physical
//condition.
Герміона вирвалася й помчала стрімголов до Сортувального Капелюха, вхопила його й
силоміць натягнула старий перелатаний шмат тканини на голову, що змусило Гаррі
здригнутися. Це Герміона пояснила була йому про Сортувальний Капелюх, проте
вона точно не ставилася до нього як до незамінного й життєво важливого
вісімсотрічного артефакту забутої магії, що невдовзі застосує складну
телепатію до її розуму, і не схоже, щоб той був у доброму фізичному стані.

// 1282
//“RAVENCLAW!”
-- РЕЙВЕНКЛОВ!

// 1283
//And talk about your foregone conclusions. Harry didn’t see why Hermione had
//been so tense about it. In what weird alternative universe would that girl
//__not__ be Sorted into Ravenclaw? If Hermione Granger didn’t go to
//Ravenclaw then there was no good reason for Ravenclaw House to exist.
І якщо вже про непослідовність. Гаррі не розумів, чому Герміона так
хвилювалася. У якому химерному альтернативному всесвіті цю дівчину __не
відсортують__ до Рейвенклову? Якщо Герміона Ґрейнджер не потрапляє до
Рейвенклову, то взагалі зникає сенс у існуванні цього гуртожитку.

// 1284
//Hermione arrived at the Ravenclaw table and got a dutiful cheer; Harry wondered
//whether the cheer would have been louder, or quieter, if they’d had any idea
//just what level of competition they’d welcomed to their table. Harry knew pi to
//3.141592 because accuracy to one part in a million was enough for most
//practical purposes. Hermione knew one hundred digits of pi because that was how
//many digits had been printed in the back of her maths textbook.
Герміона підійшла до рейвенкловського стола, де її зустріли старанними оплесками. Гаррі
зацікавило: ці вітання були б гучнішими чи тихішими, якби там мали бодай
уявлення про те, суперницю якого рівня вони вітають. Гаррі
знав, що пі дорівнює 3.141592, адже точности до мільйонної вистачає для
більшости практичних задач. Герміона памʼятала сотню цифр пі, адже саме стільки
було надруковано наприкінці її підручника з математики.

// 1285
//Neville Longbottom went to Hufflepuff, Harry was glad to see. If that House
//really did contain the loyalty and camaraderie it was supposed to exemplify,
//then a Houseful of reliable friends would do Neville a whole world of good.
//Clever kids in Ravenclaw, evil kids in Slytherin, wannabe heroes in Gryffindor,
//and everyone who does the actual work in Hufflepuff.
// часткова копія 1375
Невіл Лонґботом пішов до Гафелпафу, чому Гаррі зрадів. Якщо там справді
панують вірність і побратимство, взірцем чого мало б бути це місце, то гуртожиток
надійних друзів піде Невілові на користь. Кмітливі діти -- у Рейвенклові, лихі
-- у Слизерині, герої-мрійники -- у Ґрифіндорі, а всі, хто
по-справжньому працює, -- у Гафелпафі.

// 1286
//(Though Harry __had__ been right to consult a Ravenclaw prefect first. The
//young woman hadn’t even looked up from her reading or identified Harry, just
//jabbed a wand in Neville’s direction and muttered something. After which
//Neville had acquired a dazed expression and wandered off to the fifth carriage
//from the front and the fourth compartment on the left, which indeed had
//contained his toad.)
(Хоча Гаррі правильно зробив був, що спершу проконсультувався зі
старостою Рейвенклову. Дівчина навіть не відірвалася від своєї книжки й
не розпізнала Гаррі, просто виставила паличку в напрямку Невіла й пробурмотіла
щось. Після чого той спантеличено поплентався до пʼятого вагона з голови й
четвертого купе ліворуч, де таки знайшов свою жабу.)

// 1287
//“Malfoy, Draco!” went to Slytherin, and Harry breathed a small sigh of relief.
//It had __seemed__ like a sure thing, but you never did know what tiny
//event might upset the course of your master plan.
//копія 1469
"`Драко Мелфой!`" пішов до Слизерину, і Гаррі зітхнув із деякою полегкістю. Це
__нібито__ було безсумнівним, проте ніколи не знаєш, яка дрібна подія може
зіпсувати перебіг твого геніального плану.

// 1288
//Professor McGonagall called “Perks, Sally-Anne!”, and from the gathered
//children detached a pale waifish girl who looked oddly ethereal - like she
//might mysteriously disappear the moment you stopped looking at her, and never
//be seen again or even remembered.
-- Селі-Ен Перкс! -- гукнула професорка Макґонеґел.

Від групки дітей
відокремилася бліда, тендітна дівчинка, що мала на диво ефемерний вигляд -- ніби
вона може загадково зникнути, щойно на неї припинять дивитися, і ніколи її більше
ніхто не побачить, навіть не згадає.

// 1289
//And then (with a note of trepidation so firmly kept from her voice and face
//that you’d have needed to know her very well indeed to notice) Minerva
//McGonagall inhaled deeply, and called out, “Potter, Harry!”
А потім (із ретельно прихованим занепокоєнням у голосі, що його можна було зауважити, тільки знаючи її
надзвичайно добре) Мінерва Макґонеґел глибоко вдихнула й
викликала:

-- Гаррі Поттер!

// 1290
//There was a sudden silence in the hall.
Раптом у залі запала тиша.

// 1291
//All conversation stopped.
Усі розмови припинилися.

// 1292
//All eyes turned to stare.
Усі очі звернулися до нього.

// 1293
//For the first time in his entire life, Harry felt like he might be having an
//opportunity to experience stage fright.
Вперше в житті Гаррі відчув, що йому випала нагода пережити страх сцени, однак
// 1294
//Harry immediately stomped down this feeling. Whole room-fulls of people staring
//at him was something he’d have to accustom himself to, if he wanted to live in
//magical Britain, or for that matter do anything else interesting with his life.
//Affixing a confident and false smile to his face, he raised a foot to step
//forwards -
негайно прогнав це відчуття. Йому слід призвичаїтися до натовпів людей, що
витріщаються на нього, якщо він хоче жити в чаклунській Британії чи взагалі
досягти чогось цікавого. Гаррі зафіксував упевнену й удавану усмішку на
обличчі, підняв ногу й зробив крок уперед...

// 1295
//“Harry Potter!” cried the voice of either Fred or George Weasley, and then
//“Harry Potter!” cried the other Weasley twin, and a moment later the entire
//Gryffindor table, and soon after a good portion of Ravenclaw and Hufflepuff,
//had taken up the cry.
-- Гаррі Поттер! -- закричав Фред чи Джордж Візлі.

-- Гаррі Поттер! -- долучився до нього голос іншого близнюка Візлі, і за мить увесь ґрифіндорський стіл, а
невдовзі й значна частина рейвенкловського та гафелпафського підхопили лемент.

// 1296
//__“Harry Potter! Harry Potter! Harry Potter!__”
-- __Гаррі Поттер! Гаррі Поттер! Гаррі Поттер!__

// 1297
//And Harry Potter walked forwards. Much too slowly, he realized once he’d begun,
//but by then it was too late to alter his pace without it looking awkward.
І Гаррі Поттер пішов уперед. Надто повільно, як він збагнув, уже почавши рухатися,
проте було запізно змінювати швидкість -- це видавалося б незграбним.

***

// 1298
//“__Harry Potter! Harry Potter! HARRY POTTER!”__
-- __Гаррі Поттер! Гаррі Поттер! ГАРРІ ПОТТЕР!__

// 1299
//With all too good a notion of what she would see, Minerva McGonagall turned to
//look behind herself at the rest of the Head Table.
Хоч і надто добре уявляючи, що вона побачить, Мінерва Макґонеґел повернулася, щоб
глянути на решту вчительського стола.

// 1300
//Trelawney frantically fanning herself, Filius looking on with curiosity, Hagrid
//clapping along, Sprout looking severe, Vector and Sinistra bemused, and
//Quirrell gazing vacuously at nothing. Albus smiling benevolently. And Severus
//Snape gripping his empty wine goblet, white-knuckled, so hard that the silver
//was slowly deforming.
// часткова копія 1505
// різниця 1300/1505
// silver was slowly deforming/thick silver was slowly deforming
// Hagrid clapping/Hagrid clapping along to the music
// Vector and Sinistra bemused/ --
// Quirrell gazing vacuously at nothing / Quirrell gazing at the boy with sardonic amusement
// Albus smiling benevolently. / Directly to her left, Dumbledore humming along;
// And Severus Snape / and directly to her right, Snape
Трелоні несамовито обмахувалася, Флитвік зацікавлено спостерігав за подіями,
Геґрід плескав, Спраут суворо роздивлялася, Вектор і Сіністра сиділи приголомшені, а Квірел
дурнувато втупився в нікуди. Албус доброзичливо всміхався. А Северус Снейп
вчепився побілілими пальцями у свій порожній келих для вина так сильно,
що срібло повільно деформувалося.

// 1301
//With a wide grin, turning his head to bow to one side and then the other as he
//walked between the four House tables, Harry Potter walked forwards at a grandly
//measured pace, a prince inheriting his castle.
Гаррі Поттер широко всміхнувся, вклонився в один бік, потім в інший,
гордо простуючи між чотирма столами гуртожитків -- наче принц, що отримує в
спадок замок.

// 1302
//__“Save us from some more Dark Lords!”__ called one of the Weasley twins,
//and then the other Weasley twin cried, __“Especially if they’re
//Professors!” __to general laughter from all the tables except Slytherin.
-- __Врятуй нас ще від декількох темних лордів!__ -- заволав один із близнюків
Візлі.

А інший підхопив:

-- __Особливо якщо вони професори!__

Цю фразу зустрів регіт з усіх столів, крім слизеринського.

// 1303
//Minerva’s lips set in a white line. She would have words with the Weasley
//Horrors about that last part, if they thought she was powerless because it was
//the first day of school and Gryffindor had no points to take away. If they
//didn’t care about detentions then she would find something else.
// Майже повна копія 1514
// Різниця 1303/1514:
// Weasley Horrors/Them
// last part/last verse
// they/They
Губи Мінерви зціпилися в білу лінію. Вона ще поговорить із Пострахами Візлі щодо
цієї останньої частини, дарма вони вважають її безсилою через те, що сьогодні
перший день навчального року, тож Ґрифіндор не має очок, що їх можна було б зняти.
Якщо їм начхати на відпрацювання, вона вигадає щось інше.

// 1304
//Then, with a sudden gasp of horror, she looked in Severus’s direction,
//__surely__ he realized the Potter boy must have no idea who that was
//talking about -
// копія 1515
Раптово затамувавши подих від жаху, вона поглянула в бік Снейпа.
__Звісно__, він мусив розуміти, що маленький Поттер і гадки не мав,
про кого йшлося...

// 1305
//Severus’s face had gone beyond rage into a kind of pleasant indifference. A
//faint smile played about his lips. He was looking in the direction of Harry
//Potter, not the Gryffindor table, and his hands held the crumpled remains of a
//former wine goblet.
// копія 1516 окрім Severus/Snape
Вираз обличчя Северуса перетнув межу люті й набув відрадної байдужости. Легка усмішка
грала на його губах. Він дивився в бік Гаррі Поттера, а не ґрифіндорського
стола, а його руки досі стискали пожмакані залишки того, що колись було келихом
для вина.

***

// 1306
//Harry Potter walked forwards with a fixed smile, feeling warm inside and sort
//of awful at the same time.
Гаррі Поттер рухався вперед із закляклою усмішкою, почуваючись на душі добре й
трохи жахливо водночас.

// 1307
//They were cheering him for a job he’d done when he was one year old. A job he
//hadn’t really finished. Somewhere, somehow, the Dark Lord was still alive.
//Would they have been cheering quite so hard, if they knew that?
//копія 1519
Люди плескали йому за те, що він здійснив, коли мав лише рік. За те, чого він навіть
не довів до кінця. Десь там у якийсь спосіб Темний Лорд досі животів. Чи вітали б вони
його так само радісно, якби знали про це?

// 1308
//But the Dark Lord’s power __had __been broken once.
//копія 1520
Проте силу Темного Лорда вже було знищено одного разу.
// 1309
//And Harry would protect them again. If there was in fact a prophecy and that
//was what it said. Well, actually regardless of what any darn prophecy said.
// копія 1521
І Гаррі захистить їх знову. Якщо пророцтво справді існувало, і в ньому справді
про це йшлося. Та й узагалі -- незалежно від того, що там стверджує якесь кляте пророцтво.

// 1310
//All those people believing in him and cheering him - Harry couldn’t stand to
//let that be false. To flash and fade like so many other child prodigies. To be
//a disappointment. To fail to live up to his reputation as a symbol of the
//Light, never mind __how__ he’d gotten it. He would absolutely, positively,
//no matter how long it took and even if it killed him, fulfill their
//expectations. And then go on to __exceed__ those expectations, so that
//people wondered, looking back, that they had once asked so little of him.
//копія 1522
Всі ці люди вірили в нього й підтримували його -- Гаррі не міг допустити, щоб усе це
було даремно. Спалахнути й згаснути, як багато інших юних вундеркіндів. Виявитися розчаруванням.
Зазнати невдачі, не виправдати репутації символу Світла -- __байдуже__, як він її здобув.
Він безперечно, безумовно, хай скільки часу на це потрібно, навіть якщо це його вбʼє,
виправдає їхні сподівання. А згодом __перевершить__ ці сподівання, і люди, згадуючи
минуле, дивуватимуться, що очікували від нього так мало.

// 1311
//__“HARRY POTTER! HARRY POTTER! HARRY POTTER!”__
-- __ГАРРІ ПОТТЕР! ГАРРІ ПОТТЕР! ГАРРІ ПОТТЕР!__

// 1312
//Harry took his last steps towards the Sorting Hat. He swept a bow to the Order
//of Chaos at the Gryffindor table, and then turned and swept another bow to the
//other side of the hall, and waited for the applause and giggling to die away.
//часткова копія 1526
Гаррі зробив останні кілька кроків до Сортувального Капелюха. Уклонився Орденові
Хаосу за ґрифіндорським столом, повернувся й уклонився іншій частині
зали, а тоді зачекав, щоб оплески й хихотіння вщухли.

// 1313
//(In the back of his mind, he wondered if the Sorting Hat was genuinely
//__conscious__ in the sense of being aware of its own awareness, and if so,
//whether it was satisfied with only getting to talk to eleven-year-olds once per
//year. Its song had implied so: __Oh, I’m the Sorting Hat and I’m okay, I
//sleep all year and I work one day…__)
// @ awareness conscious
// копія 1322, 1529, тільки там без дужок і з "..." на початку
(Водночас він зацікавився, чи Сортувальний Капелюх справді був
__притомним__, тобто усвідомлював свою свідомість, і якщо так, то чи вистачало
йому розмовляти з одинадцятирічними раз на рік. Пісня ніби підтверджувала
це: "`О, я Сортувальний Капелюх, і в мене все гаразд, я сплю весь рік і працюю
лише раз...`".)

// 1314
//When there was once more silence in the room, Harry sat on the stool and
//__carefully__ placed onto his head the 800-year-old telepathic artefact of
//forgotten magic.
// копія 1530, 1323
Коли знову запала тиша, Гаррі сів на ослінчик і __обережно__ поклав
собі на голову вісімсотрічний телепатичний артефакт забутої магії.

// 1315
//Thinking, just as hard as he could: __Don’t Sort me yet! I have questions
//I need to ask you! Have I ever been Obliviated? Did you Sort the Dark Lord when
//he was a child and can you tell me about his weaknesses? Can you tell me why I
//got the brother wand to the Dark Lord’s? Is the Dark Lord’s ghost bound to my
//scar and is that why I get so angry sometimes? Those are the most important
//questions, but if you’ve got another moment can you tell me anything about how
//to rediscover the lost magics that created you?__
// копія 1324, 1531, 1542
Думаючи якомога сильніше: "`Не сортуй мене відразу! Я маю питання, що їх
треба поставити! Чи забуттятили мене? Чи сортував ти Темного Лорда, коли
він був дитиною, і чи можеш розповісти мені про його слабини? Чи відомо
тобі, чому я отримав сестру палички Темного Лорда? Чи привʼязаний
привид Темного Лорда до мого шраму й чи тому я такий злий подеколи? Це
найважливіші питання, проте якщо ти маєш ще трохи часу, то чи можеш
сказати щось про те, як віднайти втрачені чари, що дали змогу створити тебе?`".

// 1316
//Into the silence of Harry’s spirit, where before there had never been any voice
//but one, there came a second and unfamiliar voice, sounding distinctly worried:
//копія 1325
І в безгомінні свідомости Гаррі, там, де раніше завжди існував лише один голос, залунав другий,
незнайомий і, безсумнівно, збентежений:

// 1317
//__“Oh, dear. This has never happened before…”__
// ikakok помітив, що Капелюх розмовляє ніби C-3PO.
// Переклади Oh, dear:
// Частина V, 12:39: ICTV: о ні; 1+1: от лихо
// Частина V, 29:57: ICTV: який жах; 1+1: от лихо
// Частина VI, 6:33: ICTV: о ні; 1+1: лихо з тобою
// Частина VI, 28:45: ICTV: о ні; 1+1: о лихо
// Частина VI, 1:48:01: ICTV: о ні; 1+1: о лихо
// копія 1326
-- От лихо. Такого ще ніколи не траплялося...
