module.exports = {
  "staticFileGlobs": [
    "public_html/**.html",
    "public_html/hpimr.css",
    "public_html/jquery-ui.css",
    "public_html/script.js",
    "public_html/manifest.json",
    "public_html/fleur.png",
    "public_html/icon192.png",
    "public_html/cc-by-nc-sa-4.0-88x31.png"
  ],
  "stripPrefix": "public_html/",
};
